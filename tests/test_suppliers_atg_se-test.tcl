package require -exact testcl 1.0.13
namespace import ::testcl::*

# Comment in to enable logging
#log::lvSuppressLE info 0

before {
  event HTTP_REQUEST
}

###
# Test for a wrong host
###

it "should not allow wrong info" {
  on IP::client_addr return "8.8.8.8" 
  on HTTP::host return "wronghost.test.suppliers.atg.se"
  on HTTP::uri return "/init"
  endstate reject
  run test_suppliers_atg_se.irule test.suppliers.se-irule
}

###
# Test Kambi sport1
###

it "should allow kambi sport1-test.test.suppliers.atg.se" {
   # source kambi-hostheader_class.tcl
    class configure "kambi-hostheader" {
    "sport1-test.test.suppliers.atg.se" "atg-service-gateway-kambi-test3-gateway-kambi.ocptest.hh.atg.se"
    "sport1-qa.test.suppliers.atg.se" "atg-service-gateway-kambi-qa-gateway-kambi-external.ocptest.hh.atg.se"
  }
  class configure "kambi-ip" {
      "1.2.3.4" "allowed"
  }

  on IP::client_addr return "1.2.3.4" 
  on HTTP::host return "sport1-test.test.suppliers.atg.se"
  on HTTP::uri return "/init"
  verify "atg-service-gateway-kambi-test3-gateway-kambi.ocptest.hh.atg.se" "atg-service-gateway-kambi-test3-gateway-kambi.ocptest.hh.atg.se" == {HTTP::header Host}
  endstate pool ocp_dev_infranodes_443
  run test_suppliers_atg_se.irule test.suppliers.se-irule
}

it "should allow kambi sport1-qa.test.suppliers.atg.se" {
   # source kambi-hostheader_class.tcl
  class configure "kambi-hostheader" {
    "sport1-test.test.suppliers.atg.se" "atg-service-gateway-kambi-test3-gateway-kambi.ocptest.hh.atg.se"
    "sport1-qa.test.suppliers.atg.se" "atg-service-gateway-kambi-qa-gateway-kambi-external.ocptest.hh.atg.se"
  }
  class configure "kambi-ip" {
      "1.2.3.4" "allowed"
  }
  on IP::client_addr return "1.2.3.4" 
  on HTTP::host return "sport1-qa.test.suppliers.atg.se"
  on HTTP::uri return "/init"
  verify "atg-service-gateway-kambi-qa-gateway-kambi-external.ocptest.hh.atg.se" "atg-service-gateway-kambi-qa-gateway-kambi-external.ocptest.hh.atg.se" == {HTTP::header Host}
  endstate pool ocp_dev_infranodes_443
  run test_suppliers_atg_se.irule test.suppliers.se-irule
}

it "should allow kambi sport1-retail-test.test.suppliers.atg.se" {
   # source kambi-hostheader_class.tcl
  class configure "kambi-hostheader" {
    "sport1-retail-test.test.suppliers.atg.se" "atg-service-retail-gateway-kambi-test3-retail-gateway-kambi.ocptest.hh.atg.se"
    "sport1-qa.test.suppliers.atg.se" "atg-service-gateway-kambi-qa-gateway-kambi-external.ocptest.hh.atg.se"
  }
  class configure "kambi-ip" {
      "1.2.3.4" "allowed"
  }
  on IP::client_addr return "1.2.3.4" 
  on HTTP::host return "sport1-retail-test.test.suppliers.atg.se"
  on HTTP::uri return "/init"
  verify "atg-service-retail-gateway-kambi-test3-retail-gateway-kambi.ocptest.hh.atg.se" "atg-service-retail-gateway-kambi-test3-retail-gateway-kambi.ocptest.hh.atg.se" == {HTTP::header Host}
  endstate pool ocp_dev_infranodes_443
  run test_suppliers_atg_se.irule test.suppliers.se-irule
}

it "should allow kambi sport1-retail-qa.test.suppliers.atg.se" {
   # source kambi-hostheader_class.tcl
  class configure "kambi-hostheader" {
    "sport1-test.test.suppliers.atg.se" "atg-service-gateway-kambi-test3-gateway-kambi.ocptest.hh.atg.se"
    "sport1-retail-qa.test.suppliers.atg.se" "atg-service-retail-gateway-kambi-qa-retail-gateway-kambi.ocptest.hh.atg.se"
  }
  class configure "kambi-ip" {
      "1.2.3.4" "allowed"
  }
  on IP::client_addr return "1.2.3.4" 
  on HTTP::host return "sport1-retail-qa.test.suppliers.atg.se"
  on HTTP::uri return "/init"
  verify "atg-service-retail-gateway-kambi-qa-retail-gateway-kambi.ocptest.hh.atg.se" "atg-service-retail-gateway-kambi-qa-retail-gateway-kambi.ocptest.hh.atg.se" == {HTTP::header Host}
  endstate pool ocp_dev_infranodes_443
  run test_suppliers_atg_se.irule test.suppliers.se-irule
}

###
# Test Evolution casino1
###

it "should allow evolution casino1-test.test.suppliers.atg.se" {
  # source evolution-hostheader_class.tcl
  class configure "evolution-hostheader" {
    "casino1-test.test.suppliers.atg.se" "atg-service-gateway-evolution-test3-gateway-evolution.ocptest.hh.atg.se"
    "casino1-qa.test.suppliers.atg.se" "atg-service-gateway-evolution-qa-gateway-evolution-external.ocptest.hh.atg.se"
  }
  class configure "evolution-ip" {
      "1.2.3.4" "allowed"
  }

  on IP::client_addr return "1.2.3.4" 
  on HTTP::host return "casino1-test.test.suppliers.atg.se"
  on HTTP::uri return "/init"
  verify "atg-service-gateway-evolution-test3-gateway-evolution.ocptest.hh.atg.se" "atg-service-gateway-evolution-test3-gateway-evolution.ocptest.hh.atg.se" == {HTTP::header Host}
  endstate pool ocp_dev_infranodes_443
  run test_suppliers_atg_se.irule test.suppliers.se-irule
}

it "should allow evolution casino1-qa.test.suppliers.atg.se" {
  # source evolution-hostheader_class.tcl
  class configure "evolution-hostheader" {
    "casino1-test.test.suppliers.atg.se" "atg-service-gateway-evolution-test3-gateway-evolution.ocptest.hh.atg.se"
    "casino1-qa.test.suppliers.atg.se" "atg-service-gateway-evolution-qa-gateway-evolution-external.ocptest.hh.atg.se"
  }
  class configure "evolution-ip" {
      "1.2.3.4" "allowed"
  }

  on IP::client_addr return "1.2.3.4" 
  on HTTP::host return "casino1-qa.test.suppliers.atg.se"
  on HTTP::uri return "/init"
  verify "atg-service-gateway-evolution-qa-gateway-evolution-external.ocptest.hh.atg.se" "atg-service-gateway-evolution-qa-gateway-evolution-external.ocptest.hh.atg.se" == {HTTP::header Host}
  endstate pool ocp_dev_infranodes_443
  run test_suppliers_atg_se.irule test.suppliers.se-irule
}

###
# Test NetEnt casino2
###

it "should allow NetEnt casino2-test.test.suppliers.atg.se" {
  # source netent-hostheader_class.tcl
  class configure "netent-hostheader" {
    "casino2-test.test.suppliers.atg.se" "atg-service-gateway-netent-test3-gateway-netent.ocptest.hh.atg.se"
    "casino2-qa.test.suppliers.atg.se" "atg-service-gateway-netent-qa-gateway-netent-external.ocptest.hh.atg.se"
  }
  class configure "netent-ip" {
      "1.2.3.4" "allowed"
  }

  on IP::client_addr return "1.2.3.4" 
  on HTTP::host return "casino2-test.test.suppliers.atg.se"
  on HTTP::uri return "/init"
  verify "atg-service-gateway-netent-test3-gateway-netent.ocptest.hh.atg.se" "atg-service-gateway-netent-test3-gateway-netent.ocptest.hh.atg.se" == {HTTP::header Host}
  endstate pool ocp_dev_infranodes_443
  run test_suppliers_atg_se.irule test.suppliers.se-irule
}

it "should allow NetEnt casino2-qa.test.suppliers.atg.se" {
  # source netent-hostheader_class.tcl
  class configure "netent-hostheader" {
    "casino2-test.test.suppliers.atg.se" "atg-service-gateway-netent-test3-gateway-netent.ocptest.hh.atg.se"
    "casino2-qa.test.suppliers.atg.se" "atg-service-gateway-netent-qa-gateway-netent-external.ocptest.hh.atg.se"
  }
  class configure "netent-ip" {
      "1.2.3.4" "allowed"
  }

  on IP::client_addr return "1.2.3.4" 
  on HTTP::host return "casino2-qa.test.suppliers.atg.se"
  on HTTP::uri return "/init"
  verify "atg-service-gateway-netent-qa-gateway-netent-external.ocptest.hh.atg.se" "atg-service-gateway-netent-qa-gateway-netent-external.ocptest.hh.atg.se" == {HTTP::header Host}
  endstate pool ocp_dev_infranodes_443
  run test_suppliers_atg_se.irule test.suppliers.se-irule
}

###
# Test yggdrasil casino3
###

it "should allow yggdrasil casino3-test.test.suppliers.atg.se" {
  #source yggdrasil-hostheader_class.tcl
  class configure "yggdrasil-hostheader" {
    "casino3-test.test.suppliers.atg.se"  "atg-service-gateway-yggdrasil-test3-gateway-yggdrasil.ocptest.hh.atg.se"
    "casino3-qa.test.suppliers.atg.se" "atg-service-gateway-yggdrasil-qa-gateway-yggdrasil-external.ocptest.hh.atg.se"
  }
  class configure "yggdrasil-ip" {
      "1.2.3.4" "allowed"
  }
  on IP::client_addr return "1.2.3.4" 
  on HTTP::host return "casino3-test.test.suppliers.atg.se"
  on HTTP::uri return "/init"
  verify "atg-service-gateway-yggdrasil-test3-gateway-yggdrasil.ocptest.hh.atg.se" "atg-service-gateway-yggdrasil-test3-gateway-yggdrasil.ocptest.hh.atg.se" == {HTTP::header Host}
  endstate pool ocp_dev_infranodes_443
  run test_suppliers_atg_se.irule test.suppliers.se-irule
}

it "should allow yggdrasil casino3-qa.test.suppliers.atg.se" {
  #source yggdrasil-hostheader_class.tcl
  class configure "yggdrasil-hostheader" {
    "casino3-test.test.suppliers.atg.se" "atg-service-gateway-yggdrasil-test3-gateway-yggdrasil.ocptest.hh.atg.se"
    "casino3-qa.test.suppliers.atg.se" "atg-service-gateway-yggdrasil-qa-gateway-yggdrasil-external.ocptest.hh.atg.se"
  }
  class configure "yggdrasil-ip" {
      "1.2.3.4" "allowed"
  }

  on IP::client_addr return "1.2.3.4" 
  on HTTP::host return "casino3-qa.test.suppliers.atg.se"
  on HTTP::uri return "/init"
  verify "atg-service-gateway-yggdrasil-qa-gateway-yggdrasil-external.ocptest.hh.atg.se" "atg-service-gateway-yggdrasil-qa-gateway-yggdrasil-external.ocptest.hh.atg.se" == {HTTP::header Host}
  endstate pool ocp_dev_infranodes_443
  run test_suppliers_atg_se.irule test.suppliers.se-irule
}


###
# Test PlayNGo casino4
###

it "should allow PlayNGo casino4-test.test.suppliers.atg.se" {
  #source playngo-hostheader_class.tcl
  class configure "playngo-hostheader" {
    "casino4-test.test.suppliers.atg.se" "atg-service-gateway-playngo-qa-gateway-playngo-external.ocptest.hh.atg.se"
    "casino4-qa.test.suppliers.atg.se" "atg-service-gateway-playngo-qa-gateway-playngo-external.ocptest.hh.atg.se"
  }
  class configure "playngo-ip" {
      "1.2.3.4" "allowed"
  }
  on IP::client_addr return "1.2.3.4" 
  on HTTP::host return "casino4-test.test.suppliers.atg.se"
  on HTTP::uri return "/init"
  verify "atg-service-gateway-playngo-qa-gateway-playngo-external.ocptest.hh.atg.se" "atg-service-gateway-playngo-qa-gateway-playngo-external.ocptest.hh.atg.se" == {HTTP::header Host}
  endstate pool ocp_dev_infranodes_443
  run test_suppliers_atg_se.irule test.suppliers.se-irule
}

it "should allow PlayNGo casino4-qa.test.suppliers.atg.se" {
  #source playngo-hostheader_class.tcl
  class configure "playngo-hostheader" {
    "casino4-test.test.suppliers.atg.se" "atg-service-gateway-playngo-qa-gateway-playngo-external.ocptest.hh.atg.se"
    "casino4-qa.test.suppliers.atg.se" "atg-service-gateway-playngo-qa-gateway-playngo-external.ocptest.hh.atg.se"
  }
  class configure "playngo-ip" {
      "1.2.3.4" "allowed"
  }

  on IP::client_addr return "1.2.3.4" 
  on HTTP::host return "casino4-qa.test.suppliers.atg.se"
  on HTTP::uri return "/init"
  verify "atg-service-gateway-playngo-qa-gateway-playngo-external.ocptest.hh.atg.se" "atg-service-gateway-playngo-qa-gateway-playngo-external.ocptest.hh.atg.se" == {HTTP::header Host}
  endstate pool ocp_dev_infranodes_443
  run test_suppliers_atg_se.irule test.suppliers.se-irule
}

###
# Test NoLimitCity casino5
###

it "should allow NoLimitCity casino5-test.test.suppliers.atg.se" {
  source nolimitcity-hostheader_class.tcl

  on IP::client_addr return "1.2.3.4" 
  on HTTP::host return "casino5-test.test.suppliers.atg.se"
  on HTTP::uri return "/init"
  verify "atg-service-gateway-nolimitcity-test3-gateway-nolimitcity.ocptest.hh.atg.se" "atg-service-gateway-nolimitcity-test3-gateway-nolimitcity.ocptest.hh.atg.se" == {HTTP::header Host}
  endstate pool ocp_dev_infranodes_443
  run test_suppliers_atg_se.irule test.suppliers.se-irule
}

it "should allow NoLimitCity casino5-qa.test.suppliers.atg.se" {
  source nolimitcity-hostheader_class.tcl

  on IP::client_addr return "1.2.3.4" 
  on HTTP::host return "casino5-qa.test.suppliers.atg.se"
  on HTTP::uri return "/init"
  verify "atg-service-gateway-nolimitcity-qa-gateway-nolimitcity-external.ocptest.hh.atg.se" "atg-service-gateway-nolimitcity-qa-gateway-nolimitcity-external.ocptest.hh.atg.se" == {HTTP::header Host}
  endstate pool ocp_dev_infranodes_443
  run test_suppliers_atg_se.irule test.suppliers.se-irule
}

###
# Test thunder-kick casino6
###

it "should allow thunder-kick casino6-test.test.suppliers.atg.se" {
  source thunder-kick-hostheader_class.tcl

  on IP::client_addr return "1.2.3.4" 
  on HTTP::host return "casino6-test.test.suppliers.atg.se"
  on HTTP::uri return "/init"
  verify "atg-service-gateway-thunder-kick-test3-gateway-thunder-kick.ocptest.hh.atg.se" "atg-service-gateway-thunder-kick-test3-gateway-thunder-kick.ocptest.hh.atg.se" == {HTTP::header Host}
  endstate pool ocp_dev_infranodes_443
  run test_suppliers_atg_se.irule test.suppliers.se-irule
}

it "should allow thunder-kick casino6-qa.test.suppliers.atg.se" {
  source thunder-kick-hostheader_class.tcl

  on IP::client_addr return "1.2.3.4" 
  on HTTP::host return "casino6-qa.test.suppliers.atg.se"
  on HTTP::uri return "/init"
  verify "atg-service-gateway-thunder-kick-qa-gateway-thunder-kick.FELFEL.hh.atg.se" "atg-service-gateway-thunder-kick-qa-gateway-thunder-kick.ocptest.hh.atg.se" == {HTTP::header Host}
  endstate pool ocp_dev_infranodes_443
  run test_suppliers_atg_se.irule test.suppliers.se-irule
}

###
# Test elk casino7
###

it "should allow elk casino7-test.test.suppliers.atg.se" {
  source elk-hostheader_class.tcl

  on IP::client_addr return "1.2.3.4" 
  on HTTP::host return "casino7-test.test.suppliers.atg.se"
  on HTTP::uri return "/init"
  verify "atg-service-gateway-elk-test3-gateway-elk.ocptest.hh.atg.se" "atg-service-gateway-elk-test3-gateway-elk.ocptest.hh.atg.se" == {HTTP::header Host}
  endstate pool ocp_dev_infranodes_443
  run test_suppliers_atg_se.irule test.suppliers.se-irule
}

it "should allow elk casino7-qa.test.suppliers.atg.se" {
  source elk-hostheader_class.tcl

  on IP::client_addr return "1.2.3.4" 
  on HTTP::host return "casino7-qa.test.suppliers.atg.se"
  on HTTP::uri return "/init"
  verify "atg-service-gateway-elk-test3-gateway-elk.ocptest.hh.atg.se" "atg-service-gateway-elk-test3-gateway-elk.ocptest.hh.atg.se" == {HTTP::header Host}
  endstate pool ocp_dev_infranodes_443
  run test_suppliers_atg_se.irule test.suppliers.se-irule
}


###
# Test push casino8
###

it "should allow push casino8-test.test.suppliers.atg.se" {
  source push-hostheader_class.tcl

  on IP::client_addr return "1.2.3.4" 
  on HTTP::host return "casino8-test.test.suppliers.atg.se"
  on HTTP::uri return "/init"
  verify "atg-service-gateway-push-test3-gateway-push.ocptest.hh.atg.se" "atg-service-gateway-push-test3-gateway-push.ocptest.hh.atg.se" == {HTTP::header Host}
  endstate pool ocp_dev_infranodes_443
  run test_suppliers_atg_se.irule test.suppliers.se-irule
}

it "should allow push casino8-qa.test.suppliers.atg.se" {
  source push-hostheader_class.tcl

  on IP::client_addr return "1.2.3.4" 
  on HTTP::host return "casino8-qa.test.suppliers.atg.se"
  on HTTP::uri return "/init"
  verify "atg-service-gateway-push-qa-gateway-push.ocptest.hh.atg.se" "atg-service-gateway-push-qa-gateway-push.ocptest.hh.atg.se" == {HTTP::header Host}
  endstate pool ocp_dev_infranodes_443
  run test_suppliers_atg_se.irule test.suppliers.se-irule
}

###
# Test isoft casino9
###

it "should allow iSoft casino9-test.test.suppliers.atg.se" {
  source isoft-hostheader_class.tcl

  on IP::client_addr return "1.2.3.4" 
  on HTTP::host return "casino9-test.test.suppliers.atg.se"
  on HTTP::uri return "/init"
  verify "atg-service-gateway-isoft-test3-gateway-isoft.ocptest.hh.atg.se" "atg-service-gateway-isoft-test3-gateway-isoft.ocptest.hh.atg.se" == {HTTP::header Host}
  endstate pool ocp_dev_infranodes_443
  run test_suppliers_atg_se.irule test.suppliers.se-irule
}

it "should allow iSoft casino9-qa.test.suppliers.atg.se" {
  source isoft-hostheader_class.tcl

  on IP::client_addr return "1.2.3.4" 
  on HTTP::host return "casino9-qa.test.suppliers.atg.se"
  on HTTP::uri return "/init"
  verify "atg-service-gateway-isoft-qa-gateway-isoft.ocptest.hh.atg.se" "atg-service-gateway-isoft-qa-gateway-isoft.ocptest.hh.atg.se" == {HTTP::header Host}
  endstate pool ocp_dev_infranodes_443
  run test_suppliers_atg_se.irule test.suppliers.se-irule
}

###
# Test omi casino10
###

it "should allow omi casino10-test.test.suppliers.atg.se" {
  source omi-hostheader_class.tcl

  on IP::client_addr return "1.2.3.4" 
  on HTTP::host return "casino10-test.test.suppliers.atg.se"
  on HTTP::uri return "/init"
  verify "atg-service-gateway-omi-test3-gateway-omi.ocptest.hh.atg.se" "atg-service-gateway-omi-test3-gateway-omi.ocptest.hh.atg.se" == {HTTP::header Host}
  endstate pool ocp_dev_infranodes_443
  run test_suppliers_atg_se.irule test.suppliers.se-irule
}

it "should allow iSoft casino10-qa.test.suppliers.atg.se" {
  source omi-hostheader_class.tcl

  on IP::client_addr return "1.2.3.4" 
  on HTTP::host return "casino10-qa.test.suppliers.atg.se"
  on HTTP::uri return "/init"
  verify "atg-service-gateway-omi-qa-gateway-omi.ocptest.hh.atg.se" "atg-service-gateway-omi-qa-gateway-omi.ocptest.hh.atg.se" == {HTTP::header Host}
  endstate pool ocp_dev_infranodes_443
  run test_suppliers_atg_se.irule test.suppliers.se-irule
}

###
# generate Test stastistics (AND FAIL ON FAIL)
###
stats
