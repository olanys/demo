#!/bin/env python3
import yaml
from jinja2 import Template

with open('../inventory/host_vars/f5-flexdev-web/datagroups.yml','r') as f:
    yamlfile = f.read()

with open('class-template.j2','r') as f:
    classtemplate = Template(f.read())

datagroups = yaml.load(yamlfile, Loader=yaml.SafeLoader)['datagroups']
headers = [header for header in datagroups.keys() if 'hostheader' in header]

for header in headers:
    with open(f'{header}_class.tcl', 'w') as wrfile:
        dg = dict()
        for classv in datagroups[header]['strings']:
            dg[classv] = datagroups[header]['strings'][classv]
        networks = dict()
        for classv in datagroups[header]['addresses']:
            networks[classv] = "Ok"
        out = classtemplate.render(header=header, dg=dg, networks=networks)
        wrfile.write(out)
        print(f"Generated {header}_class.tcl")
