package require -exact testcl 1.0.13
namespace import ::testcl::*

# Comment in to enable logging
#log::lvSuppressLE info 0

before {
  event HTTP_REQUEST
  class configure atgse_qa_protected {
    "/admin" "nono"
  }
}

it "should use correct service for login.qa.hh.atg.se" {
  event HTTP_REQUEST
  on IP::client_addr return "10.253.0.10"
  on HTTP::uri return "/login"
  on HTTP::host return "login.qa.hh.atg.se"
  verify "atg-service-token-handler-qa-tokenservices.ocptest.hh.atg.se"  "atg-service-token-handler-qa-tokenservices.ocptest.hh.atg.se" == {HTTP::header Host}
  endstate pool ocp-dev-infra-lb
  run login_qa_atg_se.irule login-qa-atgse
}

it "should use correct service for login-tillsammans.qa.hh.atg.se" {
  event HTTP_REQUEST
  on IP::client_addr return "10.253.0.10"
  on HTTP::uri return "/login"
  on HTTP::host return "login-tillsammans.qa.hh.atg.se"
  verify "tillsammans-service-token-handler-qa-tokenservices.ocptest.hh.atg.se"  "tillsammans-service-token-handler-qa-tokenservices.ocptest.hh.atg.se" == {HTTP::header Host}
  endstate pool ocp-dev-infra-lb
  run login_qa_atg_se.irule login-qa-atgse
}

it "should block access to /admin" {
  event HTTP_REQUEST
  on IP::client_addr return "10.253.0.10"
  on HTTP::uri return "/admin/health"
  on HTTP::host return "login.qa.hh.atg.se"
  verify "Status code" 403 == {HTTP::status}
  run login_qa_atg_se.irule login-qa-atgse
}

stats
