# test-f5-config

Repo for management of iRules and Datagroups in dev/test F5 LTMs at ATG.

* yamllint is run in drone.io


###
To test a unit-test
When in the root of this repo  
`docker run -v `pwd`:/drone/src -t nexus.hh.atg.se:17000/netman/testcl:0.1 jtcl test_suppliers_atg_se-test.tcl`
